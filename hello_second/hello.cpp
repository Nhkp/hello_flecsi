/**
 * @file hello.cpp
 * @author Clément Palézis
 * @brief Hello action
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <flecsi/execution.hh>
#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "control.hpp"
#include "options.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

using namespace flecsi;

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

flog::tag tag_ld("ld");
flog::tag tag_mpi("mpi");

/**
 * @fn void hello_ld(exec::launch_domain)
 * @brief Hello World task ld (launch domain)
 * 
 * Prompts "Hello World" and the color number executing this task.
 * 
 * @param flecsi::exec::launch_domain
 */
void
hello_ld(exec::launch_domain) {
  flog::guard guard(tag_ld);

  flog(info) << "Hello World from color " << color() +1 << "/" << colors()
             << std::endl;
}

/**
 * @fn void hello_mpi()
 * @brief Hello World task MPI
 * 
 * Prompts "Hello World" and the process number executing this task.
 */
void
hello_mpi() {
  flog::guard guard(tag_mpi);

  flog(info) << "Hello World from process " << process() + 1 << "/"
             << processes() << std::endl;
}

/**
 * @fn inline const char * init()
 * @brief World task dep
 * 
 * Prompts "Initialization".
 * 
 * @return const char * 
 */
inline const char *
init() {
  return "Initialization";
}

/**
 * @fn int advance_ld()
 * @brief Advance action ld (launch domain)
 * 
 * Executes Hello task ld on a launch domain whose size is defined by
 * user at launch. Default size is 1.
 * 
 * @return int 
 */
int
advance_ld() {
  flog::guard guard(tag_ld);
  
  exec::launch_domain ld{Color(domain_size.value())};

  execute<hello_ld>(ld);

  return 0;
}
control::action<advance_ld, cp::control_point_ld> advance_ld_action;

/**
 * @fn int advance_mpi()
 * @brief Advance action MPI
 * 
 * Executes Hello task MPI on the whole launch domain through MPI.
 * 
 * @return int 
 */
int
advance_mpi() {
  flog::guard guard(tag_mpi);
  
  execute<hello_mpi, mpi>();

  return 0;
}
control::action<advance_mpi, cp::control_point_mpi> advance_mpi_action;

/**
 * @fn int advance_ld_init()
 * @brief Advance ld init action
 * 
 * Execute init task.
 * 
 * @return int 
 */
int
advance_ld_init() {
  flog::guard guard(tag_ld);
  auto init_future = execute<init>();

  flog(info) << init_future.get() << std::endl;

  return 0;
}
control::action<advance_ld_init, cp::control_point_ld> advance_ld_init_action;

/**
 * @fn int advance_mpi_init()
 * @brief Advance init MPI action
 * 
 * Execute init task.
 * 
 * @return int 
 */
int
advance_mpi_init() {
  flog::guard guard(tag_mpi);
  auto init_future = execute<init, mpi>();
  
  flog(info) << init_future.get() << std::endl;

  return 0;
}
control::action<advance_mpi_init, cp::control_point_mpi> advance_mpi_init_action;

const auto dep_ld = advance_ld_action.add(advance_ld_init_action);
const auto dep_mpi = advance_mpi_action.add(advance_mpi_init_action);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
