/**
 * @file control.hpp
 * @author Clément Palézis
 * @brief Control model specification
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>
#include <flecsi/run/control.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "options.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @enum cp
 * @brief Control points enumeration
 */
enum class cp { control_point_ld, control_point_mpi };

/**
 * @fn inline const char * operator*(cp control_point)
 * @brief Control point operator overloading
 * 
 * @param control_point 
 * @return const char* 
 */
inline const char *
operator*(cp control_point) {
  switch(control_point) {
    case cp::control_point_ld:
      return "control_point_ld";
    case cp::control_point_mpi:
      return "control_point_mpi";
  }
  flog_fatal("invalid control point");
}

/**
 * @struct control_policy
 * @brief Control policy struct defintion
 */
struct control_policy : flecsi::run::control_base {

  using control_points_enum = cp;
  struct node_policy {};

  using control = flecsi::run::control<control_policy>;

  /**
   * @fn size_t & step()
   * @brief Step function
   * 
   * Returns step_ data member
   * 
   * @return size_t& 
   */
  size_t & step() {
    return step_;
  }

  /**
   * @fn static bool cycle_control()
   * @brief Cycle control function
   * 
   * The core FleCSI control model inherits from the control policy, so that
   * any data members defined in your policy are carried with the control
   * policy instance, and can be accessed through a static interface.
   * 
   * @return true 
   * @return false 
   */
  static bool cycle_control() {
    return control::instance().step()++ < nb_iter.value();
  }

  template<auto CP>
  using control_point = flecsi::run::control_point<CP>;

  using cycle = flecsi::run::cycle<cycle_control,
    control_point<cp::control_point_ld>,
    control_point<cp::control_point_mpi>>;

  using control_points = std::tuple<cycle>;

private:
  size_t step_{0};
};

using control = flecsi::run::control<control_policy>;

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
