/**
 * @file main.cpp
 * @author Clément Palézis
 * @brief Main file
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <flecsi/execution.hh>
#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "control.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn int main(int argc, char ** argv)
 * @brief Main function
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int
main(int argc, char ** argv) {
  auto status = flecsi::initialize(argc, argv);
  status = control::check_status(status);

  if(status != flecsi::run::status::success) {
    return status < flecsi::run::status::clean ? 0 : status;
  }

  flecsi::flog::add_output_stream("clog", std::clog, true);

  status = flecsi::start(control::execute);

  flecsi::finalize();

  return status;
} // main

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
