/**
 * @file options.hpp
 * @author Clément Palézis
 * @brief User options header
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef DEFINE_OPTIONS
#define DEFINE_OPTIONS

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Launch domain size user option
 * 
 * Defines launch domain size user options. User can pass the size [0-128] he
 * wants through command line interface via --ld-size <nb> options.
 */
inline flecsi::program_option<int> domain_size("Launch Domain Options",
  "ld-size,s",
  "Specify the launch domain size [0-128].",
  {{flecsi::option_default, 1}},
  [](flecsi::any const & v, std::stringstream & ss) {
    const int value = flecsi::option_value<int>(v);
    return value > 0 && value < 129
             ? true
             : (ss << "value(" << value << ") out-of-range") && false;
  });

/**
 * @brief Iterations user option
 * 
 * Defines iterations number user options. User can pass number of iterations
 * [1-10] he wants through command line interface via --nb-iter <nb> options.
 */
inline flecsi::program_option<int> nb_iter("Control Model Options",
  "nb_iter,n",
  "Specify the number of iterations [1-10].",
  {{flecsi::option_default, 1}},
  [](flecsi::any const & v, std::stringstream & ss) {
    const int value = flecsi::option_value<int>(v);
    return value > 0 && value < 11
             ? true
             : (ss << "value(" << value << ") out-of-range") && false;
  });

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#endif

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
