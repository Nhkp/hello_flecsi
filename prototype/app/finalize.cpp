/**
 * @file finalize.cpp
 * @author Clément Palézis
 * @brief Finalize file
 * @version 0.1
 * @date 31-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "finalize.hpp"
#include "options.hpp"
#include "state.hpp"
#include "tasks/print.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

int
action::print() {

  if(z_extents.value() == 1)  {
    auto cell_field_accessor = cell_field(mesh_instance);
    flecsi::execute<task::io, flecsi::mpi>(mesh_instance, cell_field_accessor, "mesh");
    if(debug.value())
      flecsi::execute<task::print>(mesh_instance, cell_field_accessor);
  }
  else {
    auto cell_field3D_accessor = cell_field3D(mesh3D_instance);
    flecsi::execute<task::io3D, flecsi::mpi>(mesh3D_instance, cell_field3D_accessor, "mesh3D");
  }

  return 0;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
