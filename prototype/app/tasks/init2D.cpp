/**
 * @file init2D.cpp
 * @author Clément Palézis
 * @brief Init 2D task file
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "state.hpp"
#include "options.hpp"
#include "tasks/init2D.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::init_red2D(mesh::accessor<wo> mesh_accessor,
  field<double>::accessor<wo, wo> field_accessor) {
  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);
  for(const auto j : mesh_accessor.cells<
    mesh::y_axis, mesh::domain::interior>()) {
    forall(i, (mesh_accessor.red<
      mesh::x_axis, mesh::domain::interior>(j)), "red") {
      val[j][i] = 1;
    };
  }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::init_black2D(mesh::accessor<wo> mesh_accessor,
  field<double>::accessor<wo, wo> field_accessor) {
  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);
  for(const auto j : mesh_accessor.cells<
    mesh::y_axis, mesh::domain::interior>()) {
    forall(i, (mesh_accessor.black<
      mesh::x_axis, mesh::domain::interior>(j)), "black") {
      val[j][i] = 0;
    };
  }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void task::init_mesh2D() {
  flog(info) << "Mesh (" << x_extents.value() << "x" << y_extents.value()
             << ") initialization" << std::endl;
  flecsi::flog::flush();

  std::vector<std::size_t> axis_extents{x_extents.value(), y_extents.value()};

  // Distribute the number of processes over the axis colors.
  auto axis_colors = mesh::distribute(flecsi::processes(), axis_extents);

  coloring.allocate(axis_colors, axis_extents);

  mesh::grect geometry;
  geometry[0][0] = 0.0;
  geometry[0][1] = 1.0;
  geometry[1] = geometry[0];

  mesh_instance.allocate(coloring.get(), geometry);

  auto cell_field_accessor = cell_field(mesh_instance);

  flecsi::execute<task::init_red2D>(mesh_instance, cell_field_accessor);
  flecsi::execute<task::init_black2D>(mesh_instance, cell_field_accessor);
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
