/**
 * @file modify.cpp
 * @author Clément Palézis
 * @brief Modify task file
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "tasks/modify.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::modify(mesh::accessor<ro> mesh_accessor,
  field<double>::accessor<rw, rw> field_accessor) {
  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);
  for(const auto j : mesh_accessor.cells<
    mesh::y_axis, mesh::domain::interior>()) {
    forall(i, (mesh_accessor.red<
      mesh::x_axis, mesh::domain::interior>(j)), "red") {
      val[j][i] += 2;
    };
  }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::modify3D(mesh3D::accessor<ro> mesh_accessor,
  field<double>::accessor<rw, rw, rw> field_accessor) {
  auto val = mesh_accessor.mdspan<mesh3D::cells>(field_accessor);
  for(const auto k : mesh_accessor.cells<mesh3D::z_axis>()) {
    for(const auto j : mesh_accessor.cells<mesh3D::y_axis>()) {
      forall(i, mesh_accessor.red<mesh3D::x_axis>(j), "red") {
        val[k][j][i] += 1;
      };
    }
  }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
