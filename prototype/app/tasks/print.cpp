/**
 * @file print.cpp
 * @author Clément Palézis
 * @brief Print task file
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>
#include <sstream>
#include <filesystem>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "tasks/print.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::print(mesh::accessor<ro> mesh_accessor,
  field<double>::accessor<ro, ro> field_accessor) {

  flecsi::flog::guard guard(task::tag_finalize);

  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);
  for(const auto i : mesh_accessor.cells<mesh::y_axis>()) {
    for(const auto j : mesh_accessor.cells<mesh::x_axis>()) {
      flog(info) << "cell (" << i << ", " << j << ") has value "
        << val[i][j] << std::endl;
    };
  }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::io(mesh::accessor<ro> mesh_accessor,
  field<double>::accessor<ro, ro> field_accessor,
  std::string filebase) {
  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);

  std::stringstream ss;
  ss << filebase;
  if(flecsi::processes() == 1) {
    ss << ".dat";
  }
  else {
    ss << "-" << flecsi::process() << ".dat";
  }

  std::cout << "Removing old file : " << ss.str() << "... ";
  try {
    if(std::filesystem::remove(ss.str())) {
      std::cout << "Deleted !" << std::endl;
    }
    else {
      std::cout << "Not found !" << std::endl;
    }
  }
  catch(const std::filesystem::filesystem_error& err) {
    std::cout << "filesystem error: " << err.what() << std::endl;
  }
  std::cout << "Creating new file : " << ss.str() << std::endl;
  std::ofstream file(ss.str(), std::ofstream::out);

  for(auto j : mesh_accessor.cells<mesh::y_axis>()) {
    for(auto i : mesh_accessor.cells<mesh::x_axis>()) {
      file << val[j][i] << "\t";
    }
    file << std::endl;
  }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::io3D(mesh3D::accessor<ro> mesh_accessor,
  field<double>::accessor<ro, ro, ro> field_accessor,
  std::string filebase) {
  auto val = mesh_accessor.mdspan<mesh3D::cells>(field_accessor);

  std::stringstream ss;
  ss << filebase;
  if(flecsi::processes() == 1) {
    ss << ".dat";
  }
  else {
    ss << "-" << flecsi::process() << ".dat";
  }

  std::cout << "Removing old file : " << ss.str() << "... ";
  try {
    if(std::filesystem::remove(ss.str())) {
      std::cout << "Deleted !" << std::endl;
    }
    else {
      std::cout << "Not found !" << std::endl;
    }
  }
  catch(const std::filesystem::filesystem_error& err) {
    std::cout << "filesystem error: " << err.what() << std::endl;
  }
  std::cout << "Creating new file : " << ss.str() << std::endl;
  std::ofstream file(ss.str(), std::ofstream::out);

  for(auto k : mesh_accessor.cells<mesh3D::z_axis>()) {
    file << "z = " << k << "\n" << std::endl;

    for(auto j : mesh_accessor.cells<mesh3D::y_axis>()) {
      for(auto i : mesh_accessor.cells<mesh3D::x_axis>()) {
        file << val[k][j][i] << "\t";
      }
      file << std::endl;
    }
    file << std::endl;
  }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
