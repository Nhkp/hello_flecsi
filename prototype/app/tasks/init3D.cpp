/**
 * @file init3D.cpp
 * @author Clément Palézis
 * @brief Init 3D task file
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "state.hpp"
#include "options.hpp"
#include "tasks/init3D.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::init_red3D(mesh3D::accessor<wo> mesh_accessor,
  field<double>::accessor<wo, wo, wo> field_accessor) {
  auto val = mesh_accessor.mdspan<mesh3D::cells>(field_accessor);
  for(const auto k : mesh_accessor.cells<mesh3D::z_axis>()) {
    for(const auto j : mesh_accessor.cells<mesh3D::y_axis>()) {
      forall(i, mesh_accessor.red<mesh3D::x_axis>(j), "red") {
        val[k][j][i] = 1;
      };
    }
  }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::init_black3D(mesh3D::accessor<wo> mesh_accessor,
  field<double>::accessor<wo, wo, wo> field_accessor) {
  auto val = mesh_accessor.mdspan<mesh3D::cells>(field_accessor);
  for(const auto k : mesh_accessor.cells<mesh3D::z_axis>()) {
    for(const auto j : mesh_accessor.cells<mesh3D::y_axis>()) {
      forall(i, mesh_accessor.black<mesh3D::x_axis>(j), "black") {
        val[k][j][i] = 0;
      };
    }
  }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::init_mesh3D() {
  flog(info) << "Mesh (" << z_extents.value() << "x" << y_extents.value()
             << "x" << x_extents.value() << ") initialization" << std::endl;
  flecsi::flog::flush();

  std::vector<std::size_t> axis_extents{x_extents.value(),
    y_extents.value(),
    z_extents.value()
    };

  // Distribute the number of processes over the axis colors.
  auto axis_colors = mesh3D::distribute(flecsi::processes(), axis_extents);

  coloring3D.allocate(axis_colors, axis_extents);

  mesh3D::grect geometry;
  geometry[0][0][0] = 0.0;
  geometry[0][0][1] = 1.0;
  geometry[0][1][0] = 0.0;
  geometry[0][1][1] = 1.0;
  geometry[1] = geometry[0];

  mesh3D_instance.allocate(coloring3D.get(), geometry);

  auto cell_field_accessor = cell_field3D(mesh3D_instance);

  flecsi::execute<task::init_red3D>(mesh3D_instance, cell_field_accessor);
  flecsi::execute<task::init_black3D>(mesh3D_instance, cell_field_accessor);
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
