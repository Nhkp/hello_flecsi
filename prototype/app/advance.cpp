/**
 * @file advance.cpp
 * @author Clément Palézis
 * @brief Advance file
 * @version 0.1
 * @date 31-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "advance.hpp"
#include "state.hpp"
#include "tasks/modify.hpp"
#include "specialization/control.hpp"
#include "options.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

int
action::advance_init() {
  flecsi::flog::guard guard(task::tag_advance);
  
  auto init_future = flecsi::execute<task::advance_init, flecsi::mpi>();
  
  flog(info) << init_future.get() << "(" << control::policy().step() << "/"
    << nb_iter.value() << ")"<< std::endl;

  return 0;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

int
action::advance() {

  if(z_extents.value() == 1)  {
    auto cell_field_accessor = cell_field(mesh_instance);
    flecsi::execute<task::modify, flecsi::mpi>(mesh_instance, cell_field_accessor);
  }
  else {
    auto cell_field3D_accessor = cell_field3D(mesh3D_instance);
    flecsi::execute<task::modify3D, flecsi::mpi>(mesh3D_instance, cell_field3D_accessor);
  }

  return 0;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
