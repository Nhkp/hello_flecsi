/**
 * @file modify.hpp
 * @author Clément Palézis
 * @brief Modify task header
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "specialization/types.hpp"
#include "specialization/mesh.hpp"
#include "specialization/mesh3D.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

namespace task {

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

inline flecsi::flog::tag tag_advance("advance");

/**
 * @fn void modify(mesh::accessor<ro> mesh_accessor,
 *  field<double>::accessor<rw, rw> field_accessor)
 * @brief Modify task
 * 
 * Modify red cells value.
 * 
 * @param mesh_accessor (mesh::accessor<ro>)
 * @param field_accessor (field<double>::accessor<rw, rw>)
 */
void modify(mesh::accessor<ro> mesh_accessor,
  field<double>::accessor<rw, rw> field_accessor);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn void modify3D(mesh3D::accessor<ro> mesh_accessor,
 *  field<double>::accessor<rw, rw, rw> field_accessor)
 * @brief Modify task
 * 
 * Modify red cells value.
 * 
 * @param mesh_accessor (mesh3D::accessor<ro>)
 * @param field_accessor (field<double>::accessor<rw, rw>)
 */
void modify3D(mesh3D::accessor<ro> mesh_accessor,
  field<double>::accessor<rw, rw, rw> field_accessor);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn inline const char * init()
 * @brief Advance init task dep
 * 
 * Returns "Advance initialization".
 * 
 * @return const char * 
 */
inline const char *
advance_init() {
  return "Advance...";
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

} // namespace task

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
