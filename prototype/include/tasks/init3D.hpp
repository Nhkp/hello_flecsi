/**
 * @file init3D.hpp
 * @author Clément Palézis
 * @brief Init 3D task header
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "specialization/types.hpp"
#include "specialization/mesh.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

namespace task {

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn void init_red3D(mesh::accessor<wo> mesh_accessor,
 *  field<double>::accessor<wo, wo> field_accessor)
 * @brief Init red task
 * 
 * Assign 0 to red cells of the mesh.
 * 
 * @param mesh_accessor (mesh::accessor<wo>)
 * @param field_accessor (field<double>::accessor<wo, wo>)
 */
void init_red3D(mesh3D::accessor<wo> mesh_accessor,
  field<double>::accessor<wo, wo, wo> field_accessor);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn void init_black3D(mesh::accessor<wo> mesh_accessor,
 *  field<double>::accessor<wo, wo> field_accessor)
 * @brief Init black task
 * 
 * Assign 0 to black cells of the mesh.
 * 
 * @param mesh_accessor (mesh::accessor<wo>)
 * @param field_accessor (field<double>::accessor<wo, wo>)
 */
void init_black3D(mesh3D::accessor<wo> mesh_accessor,
  field<double>::accessor<wo, wo, wo> field_accessor);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn void init_mesh3D()
 * @brief Init 3D mesh function
 * 
 * Allocates a coloring and a mesh. Executes init color tasks.
 */
void init_mesh3D();

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

} // namespace task

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
