/**
 * @file print.hpp
 * @author Clément Palézis
 * @brief Print task header
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "specialization/types.hpp"
#include "specialization/mesh.hpp"
#include "specialization/mesh3D.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

namespace task {

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

inline flecsi::flog::tag tag_finalize("finalize");

/**
 * @fn void print(mesh::accessor<ro> mesh_accessor,
 *  field<double>::accessor<ro, ro> field_accessor)
 * @brief Print task
 * 
 * Print cells value.
 * 
 * @param mesh_accessor (mesh::accessor<ro>)
 * @param field_accessor (field<double>::accessor<ro, ro>)
 */
void print(mesh::accessor<ro> mesh_accessor,
  field<double>::accessor<ro, ro> field_accessor);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn void io(mesh::accessor<ro> mesh_accessor,
 *   field<double>::accessor<ro, ro> field_accessor,
 *   std::string filebase)
 * @brief IO function
 * 
 * Print cells value in a file. Produce one file per process.
 * 
 * @param mesh_accessor (mesh::accessor<ro>)
 * @param field_accessor (field<double>::accessor<ro, ro>)
 * @param filebase (std::string)
 */
void io(mesh::accessor<ro> mesh_accessor,
  field<double>::accessor<ro, ro> field_accessor,
  std::string filebase);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn void io3D(mesh3D::accessor<ro> mesh_accessor,
 *   field<double>::accessor<ro, ro, ro> field_accessor,
 *   std::string filebase)
 * @brief IO function
 * 
 * Print cells value in a file. Produce one file per process.
 * 
 * @param mesh_accessor (mesh3D::accessor<ro>)
 * @param field_accessor (field<double>::accessor<ro, ro, ro>)
 * @param filebase (std::string)
 */
void io3D(mesh3D::accessor<ro> mesh_accessor,
  field<double>::accessor<ro, ro, ro> field_accessor,
  std::string filebase);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

} // namespace task

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
