/**
 * @file mesh.hpp
 * @author Clément Palézis
 * @brief Mesh specification file
 * @version 0.1
 * @date 29-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/data.hh>
#include <flecsi/topo/narray/coloring_utils.hh>
#include <flecsi/topo/narray/interface.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "types.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @struct mesh
 * @brief Mesh
 * 
 * Structured mesh
 */
struct mesh : flecsi::topo::specialization<flecsi::topo::narray, mesh> {

  /*-----------------------------------------------------------------------*
    Policy Information.
   *-----------------------------------------------------------------------*/

  enum index_space { cells };
  using index_spaces = mesh::has<cells>;

  enum axis { x_axis, y_axis };
  using axes = mesh::has<x_axis, y_axis>;

  enum domain { all, interior };
  enum boundary { low, high };

  struct meta_data {
    double xdelta;
    double ydelta;
  };

  static constexpr std::size_t dimension = 2;

  template<auto>
  static constexpr std::size_t privilege_count = 2;

  /*-----------------------------------------------------------------------*
    Interface.
   *-----------------------------------------------------------------------*/

  /**
   * @struct interface
   * @brief Interface struct
   * 
   * A templated interface to manipulate the mesh.
   * 
   * @tparam B (class)
   */
  template<class B>
  struct interface : B {

    /**
     * @fn std::size_t size()
     * @brief Size function
     * 
     * A templated function returning the size of a given domain along a
     * given axis.
     * 
     * @tparam A (axis)
     * @tparam DM (domain, default = interior)
     * @return std::size_t 
     */
    template<axis A, domain DM = all>
    std::size_t size() {
      if(DM == all) {
        return B::template size<index_space::cells, A, B::domain::all>();
      }
    }

    /**
     * @fn std::size_t global_id(std::size_t i) const
     * @brief Global ID function
     * 
     * A templated function returning the ID of a cell along a given axis.
     * 
     * @tparam A (axis)
     * @param i (std::size_t)
     * @return std::size_t 
     */
    template<axis A>
    std::size_t global_id(std::size_t i) const {
      return B::template global_id<index_space::cells, A>(i);
    }

    /**
     * @fn std::size_t extents() const
     * @brief Extents function
     * 
     * Returns the size of the index space along a given axis.
     * 
     * @tparam A (axis)
     * @return std::size_t 
     */
    template<axis A>
    std::size_t extents() const {
      return B::template extents<index_space::cells, A>();
    }


    /**
     * @fn auto cells()
     * @brief Cells functions
     * 
     * Returns cells' range index space.
     * 
     * @tparam A (axis)
     * @return auto 
     */
    template<axis A, domain DM = all>
    auto cells() {
      if(DM == all) {
        return B::template range<index_space::cells, A, B::domain::all>();
      }
      else if(DM == interior) {
        return B::
          template range<index_space::cells, A, B::domain::logical>();
      }
    }

    /**
     * @fn auto red(std::size_t row)
     * @brief Red function
     * 
     * A templated function returning red cells ID's along a given axis.
     * 
     * @tparam A (axis)
     * @param row (std::size_t)
     * @return auto 
     */
    template<axis A, domain DM = all>
    auto red(std::size_t row) {
      if(DM == all) {
        // const bool low = B::template is_low<index_space::cells, A>();
        // const bool high = B::template is_high<index_space::cells, A>();
        const bool low  = false;
        const bool high = false;
        
        const std::size_t start =
          B::template extended<index_space::cells, A, 0>();
        const std::size_t end =
          B::template extended<index_space::cells, A, 1>();
        
        const std::size_t rng = (end - high) - (start + low);

        // const bool parity = (0 == global_id<A>(start + low) % 2);
        const bool parity = (0 == extents<A>() % 2);

        // clang-format off
        const std::size_t pts =
          (0 == rng % 2) ? // even number of red and black points
            rng / 2 :
            parity == (0 == (row + (1 - parity)) % 2) ? // more red than black
              (rng + 1) / 2 :
              (rng - 1) / 2; // fewer red than black
        // clang-format on

        return flecsi::topo::make_stride_ids<index_space::cells, 2>(
          flecsi::util::iota_view<flecsi::util::id>(0, pts),
          start + low,
          (row + parity) % 2);
      }
      else if(DM == interior){
        const bool interior = B::template is_interior<index_space::cells, A>();
        
        const std::size_t start =
          B::template logical<index_space::cells, A, 0>();
        const std::size_t end =
          B::template logical<index_space::cells, A, 1>();
        
        const std::size_t rng = (end - interior) - (start + interior);

        const bool parity = (0 == global_id<A>(start + interior) % 2);
        // const bool parity = (0 == extents<A>() % 2);

        // clang-format off
        const std::size_t pts =
          (0 == rng % 2) ? // even number of red and black points
            rng / 2 :
            parity == (0 == (row + (1 - parity)) % 2) ? // more red than black
              (rng + 1) / 2 :
              (rng - 1) / 2; // fewer red than black
        // clang-format on

        return flecsi::topo::make_stride_ids<index_space::cells, 2>(
          flecsi::util::iota_view<flecsi::util::id>(0, pts),
          start + interior,
          (row + parity) % 2);
      }
    }

    /**
     * @fn auto black(std::size_t row)
     * @brief Black function
     * 
     * A templated function returning black cells ID's along a given axis.
     * 
     * @tparam A (axis)
     * @param row (std::size_t)
     * @return auto 
     */
    template<axis A, domain DM = all>
    auto black(std::size_t row) {
      return red<A, DM>(row + 1);
    }

    /**
     * @fn double xdelta()
     * @brief Xdelta function
     * 
     * A function returning the xdelta value.
     * 
     * @return double 
     */
    double xdelta() {
      return (*(this->policy_meta_)).xdelta;
    }

    /**
     * @fn double ydelta()
     * @brief Ydelta function
     * 
     * A function returning the ydelta value.
     * 
     * @return double 
     */
    double ydelta() {
      return (*(this->policy_meta_)).ydelta;
    }

  }; // struct interface

  /**
   * @fn static auto distribute(std::size_t np,
   *       std::vector<std::size_t> indices)
   * @brief Distribute function
   * 
   * A function distributing the number of processes along given indices.
   * 
   * @param np (std::size_t)
   * @param indices (std::vector<std::size_t>)
   * @return auto 
   */
  static auto distribute(std::size_t np, std::vector<std::size_t> indices) {
    return flecsi::topo::narray_utils::distribute(np, indices);
  }

  /*-----------------------------------------------------------------------*
    Color Method.
   *-----------------------------------------------------------------------*/
  
  /**
   * @fn static coloring color(colors axis_colors, coord, axis_extents)
   *  @brief Color function
   * 
   * A function coloring the mesh with a given color along a given axis.
   * 
   * @param axis_colors (colors)
   * @param axis_extents (colors)
   * @return coloring 
   */
  static coloring
  color(base::colors axis_colors, base::coord axis_extents) {
    base::coord hdepths{1, 1};
    base::coord bdepths{1, 1};
    std::vector<bool> periodic{false, false};
    base::coloring_definition cd{
      axis_colors, axis_extents, hdepths, bdepths, periodic};

    auto [nc, ne, pcs, partitions] =
      flecsi::topo::narray_utils::color(cd, MPI_COMM_WORLD);

    flog_assert(nc == flecsi::processes(),
      "current implementation is restricted to 1-to-1 mapping");

    coloring c;
    c.comm = MPI_COMM_WORLD;
    c.colors = nc;
    c.idx_colorings.emplace_back(std::move(pcs));
    c.partitions.emplace_back(std::move(partitions));
    return c;
  }

  /*-----------------------------------------------------------------------*
    Initialization.
   *-----------------------------------------------------------------------*/

  using grect = std::array<std::array<double, 2>, 2>;

  /**
   * @fn static void set_geometry(mesh::accessor<flecsi::rw> sm,
   *       grect const & g)
   * @brief Set Geometry function
   * 
   * A task function initializing the mesh geometry.
   * 
   * @param sm (mesh::accessor<rw>)
   * @param g (std::array<std::array<double, 2>, 2> const &)
   */
  static void set_geometry(mesh::accessor<flecsi::rw> sm,
    grect const & g) {
    meta_data & md = sm.policy_meta_;
    double xdelta =
      std::abs(g[0][1] - g[0][0]) / (sm.size<x_axis, all>() - 1);
    double ydelta =
      std::abs(g[1][1] - g[1][0]) / (sm.size<y_axis, all>() - 1);

    md.xdelta = xdelta;
    md.ydelta = ydelta;
  }

  /**
   * @fn static void initialize(flecsi::data::topology_slot<mesh> & s,
           coloring const &,
           grect const & geometry)
   * @brief Initialize function
   * 
   * A wrapping function launching set_geometry through mpi processes.
   * 
   * @param s (data::topology_slot<mesh> &)
   * @param geometry (grect const &)
   */
  static void initialize(flecsi::data::topology_slot<mesh> & s,
    coloring const &,
    grect const & geometry) {
    flecsi::execute<set_geometry, flecsi::mpi>(s, geometry);
  }

}; // struct mesh

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
