/**
 * @file state.hpp
 * @author Clément Palézis
 * @brief State header
 * @version 0.1
 * @date 01-04-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "specialization/mesh.hpp"
#include "specialization/mesh3D.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

inline const field<double>::definition<mesh, mesh::cells> cell_field;

inline mesh::slot mesh_instance;
inline mesh::cslot coloring;

inline const field<double>::definition<mesh3D, mesh3D::cells> cell_field3D;

inline mesh3D::slot mesh3D_instance;
inline mesh3D::cslot coloring3D;

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
