/**
 * @file options.hpp
 * @author Clément Palézis
 * @brief User options file
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Iterations user option
 * 
 * Defines iterations number user options. User can pass number of iterations
 * [1-10] he wants through command line interface via --nb-iter <nb> options.
 */
inline flecsi::program_option<int> nb_iter("Control Model Options",
  "nb_iter,n",
  "Specify the number of iterations [1-10].",
  {{flecsi::option_default, 1}},
  [](flecsi::any const & v, std::stringstream & ss) {
    const int value = flecsi::option_value<int>(v);
    return value > 0 && value < 11
             ? true
             : (ss << "value(" << value << ") out-of-range") && false;
  });

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief X size user option
 * 
 * Defines x size user options.
 */
inline flecsi::program_option<std::size_t> x_extents("x-extents",
  "The x extents of the mesh [1-32].",
  1);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Y size user option
 * 
 * Defines y size user options.
 */
inline flecsi::program_option<std::size_t> y_extents("y-extents",
  "The x extents of the mesh [1-32].",
  1);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Z size user option
 * 
 * Defines z size user options.
 */
inline flecsi::program_option<std::size_t> z_extents("Mesh Options",
  "z-extents,z",
  "The x extents of the mesh [1-32].",
  {{flecsi::option_default, 1}},
  [](flecsi::any const & v, std::stringstream & ss) {
    const int value = flecsi::option_value<std::size_t>(v);
    return value > 0 && value < 33
             ? true
             : (ss << "value(" << value << ") out-of-range") && false;
  });

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Debug option
 * 
 * Enable debugging prints.
 */
inline flecsi::program_option<bool> debug("Debug Options",
  "debug,d",
  "Enable debugging mode.",
  {{flecsi::option_default, false}, {flecsi::option_implicit, true}}
  );

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
