/**
 * @file control.hpp
 * @author Clément Palézis
 * @brief Control model specification
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>
#include <flecsi/run/control.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @enum cp
 * @brief Control points enumeration
 */
enum class cp { advance };

/**
 * @fn inline const char * operator*(cp control_point)
 * @brief Control point operator overloading
 * 
 * @param control_point 
 * @return const char* 
 */
inline const char *
operator*(cp control_point) {
  switch(control_point) {
    case cp::advance:
      return "advance";
  }
  flog_fatal("invalid control point");
}

/**
 * @struct control_policy
 * @brief Control policy struct definition
 */
struct control_policy : flecsi::run::control_base {

  using control_points_enum = cp;
  struct node_policy {};

  using control = flecsi::run::control<control_policy>;

  template<auto CP>
  using control_point = flecsi::run::control_point<CP>;

  using control_points = std::tuple<control_point<cp::advance>>;
};

using control = flecsi::run::control<control_policy>;

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
