/**
 * @file hello.cpp
 * @author Clément Palézis
 * @brief Hello action
 * @version 0.1
 * @date 24-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <flecsi/execution.hh>
#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "control.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

using namespace flecsi;

/**
 * @fn void hello()
 * @brief Hello task
 * 
 * Prompts "Hello World".
 * 
 * @return void
 */
void
hello() {
  flog(info) << "Hello World" << std::endl;
}

/**
 * @fn int advance()
 * @brief Advance action
 * 
 * Execute Hello task.
 * 
 * @return int 
 */
int
advance() {
  
  execute<hello>();

  return 0;
}
control::action<advance, cp::advance> advance_action;

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
