# Hello_FleCSI

**Hello_FleCSI**: a "Hello World" standalone to discover FleCSI framework.

## Getting started

### Getting the code

Clone the repository

    git clone https://gitlab.com/Nhkp/hello_flecsi.git

### Dependencies

- GCC 9.4.0/Clang 8.0.1+
- CMake 3.20+
- FleCSI 2.1.0
- Legion 21.03.0
- Kokkos 3.5.00
- Doxygen
- parMETIS 4.0.3/METIS 5.1.0
- Boost 1.70.0
- OpenMPI 3.1.6

### Contents

- `hello_first`: basic "Hello World" implementation.
- `hello_second`: distributed "Hello World" implementation.
- `prototype`: basic standalone implementation working on a 2D/3D structured mesh.
  - `app`: source files including `tasks`.
  - `include`: source headers including `tasks` and `specialization`.

## Usage

### Documentation

    doxygen Doxyfile

### Configuration

    cmake -S . -B build

### Compilation

    make -C build [-j 16]

### Execution

#### hello-first

    ./build/hello-first

#### hello-second

    [mpiexec -n <nb>] ./build/hello-second [--ld-size <nb>] [--flog-process=-1]

#### prototype

    [mpiexec -n <nb>] ./build/prototype/prototype <x> <y> [--z-extents <z>] [--nb-iter <nb>] [--debug] [--flog-process=-1]
